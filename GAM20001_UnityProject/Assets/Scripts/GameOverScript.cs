﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour 
{
	public Text scoreText;


	void Awake()
	{
		scoreText.text = "Score - " + PlayerPrefs.GetInt("score").ToString();
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;
	}


	public void StartAgain(string levelName)
	{
		Application.LoadLevel(levelName);
	}
}
